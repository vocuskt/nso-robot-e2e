# NSO Dockerized with Robot Framework prototype

This project is a test project used as an example to execute [robot framework](https://robotframework.org/) testing against a complete NSO image that contains NSO service packages and NEDs. 
This is an extension of NSO in Docker (NID) ecosystem initiative project created by Cisco to enable users of NSO to easily run NSO in Docker.
More information on NID ecosystem [here](https://gitlab.com/nso-developer/nso-docker).

In essence, NID ecosystem will have 3 (or more) repositories, each one producing their own image: 
* base: NSO base - example: https://gitlab.com/vocuskt/nso-docker
* ned: Individual NEDs  - example: https://gitlab.com/vocuskt/iosxr-ned or https://gitlab.com/vocuskt/ios-ned
* system: base + NEDs + custom service packages where base and NED are just images pulled from the first 2 repositories - https://gitlab.com/vocuskt/nso-service-package 

To maintain the modularity of NID ecosystem, rather than modifying system repository itself to include robot framework testing for test script automation, this project is built instead. 

This project uses docker-compose to pull image built in system repository then pull image of netsim devices, and pull robot framework image and executing the robot test cases. As a result, a robot result report is produced as an artifact of a pipeline job. 

## File structure

* docker-composed.yml: Simple docker composed file for local development.
* test: This folder contains robot test scripts and will be triggered by a robot framework image.
* result: Just an empty folder as placeholder to build result report artifact after robot testing execution. 
* nso: post-start related script to onboard netsim devices of our choosing based on our testing scenarios. This can be further extended to include onboarding of real device in labs (could make use of docker-networking), or even spins up devices in CML from another image.

### Docker-compose

Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services.

The composed file used for this project looks like 

```yaml
---
version: '3'

services:
  # Pulls the netsim images built by NEDs repository
  devIOS-0:
    container_name: devIOS-0
    image: registry.gitlab.com/vocuskt/ios-ned/netsim-cisco-ios-cli-6.40:5.3.4.1
  devIOS-1:
    container_name: devIOS-1
    image: registry.gitlab.com/vocuskt/ios-ned/netsim-cisco-ios-cli-6.40:5.3.4.1
  # Pulls the complete NSO image with custom service packages
  nso:
    container_name: nso
    image: registry.gitlab.com/vocuskt/nso-service-package/nso:5.3.4.1
    environment:
    - HTTP_ENABLE=true
    - HTTPS_ENABLE=true
    - ADMIN_PASSWORD=admin
    # After NSO up and running, execute this script to onboard netsim devices into NSO
    volumes:
    - "./nso/post-start:/etc/ncs/post-ncs-start.d/"
    ports:
    - 80:80
    - 443:443
  tests:
    # This is your Robot test image. It will run the test like if it was an external service
    image: registry.gitlab.com/vocuskt/robot-framework-docker:latest
    container_name: mytests
    depends_on:
    - nso
    # Executes robot scripts in /test and produce the result in /results which is generated as artifact after pipeline runs
    volumes:
    - "./test:/test"
    - "./results:/results"
```

## running the tests (Interactive)

This is basically the commands ran by the pipeline

```
docker-compose -f docker-compose.yml run tests
```

## Run in local environment

You can use it as a dev local stack.
Use VSCode if you want to use code directly inside the container.
The test scripts are mounted as a bind volume. So you can directly modify the them locally.

```
docker-compose up -d
```

### Test results

The test results are saved in /results if you're running docker compose locally - or download the artifact after pipeline job finishes. 

### Running multiple NSOs

You could modify the pipeline to run multiple NSOs instance (E.g LSA)
For that, just create two folder (Same format as the nso folder) and remove the nso folder.

Then create two service(cfs, rfs) in docker compose and link them to the right folder.

You can also use the pre-start scripts and post-start script to modify the configuration.

Example 
```yaml
---
version: '3'

services:
  cfs:
    container_name: cfs
    # This is your NSO base (dev) Image. The one you use to compile the packages
    image: <cfs-image>
    environment:
    - HTTP_ENABLE=true
    - HTTPS_ENABLE=true
    - ADMIN_PASSWORD=admin
    volumes:
    # Run scripts when NSO is started (e.g netsim devices)
    - "./cfs/netsim:/etc/ncs/post-ncs-start.d/"
    # Run the scripts before NSO is started (e.g load users)
    - "./cfs/pre-start:/etc/ncs/pre-ncs-start.d/"
    # You don't need to expose these ports in a pipeline. But it can be useful for development
    ports:
    - 80:80
    - 443:443
  rfs:
    container_name: rfs
    # This is your NSO base (dev) Image. The one you use to compile the packages
    image: <rfs-image>
    environment:
    - HTTP_ENABLE=true
    - HTTPS_ENABLE=true
    - ADMIN_PASSWORD=admin
    volumes:
    # Run scripts when NSO is started (e.g netsim devices)
    - "./rfs/netsim:/etc/ncs/post-ncs-start.d/"
    # Run the scripts before NSO is started (e.g load users)
    - "./rfs/pre-start:/etc/ncs/pre-ncs-start.d/"
    # You don't need to expose these ports in a pipeline. But it can be useful for development
    ports:
    - 80:80
    - 443:443
  tests:
    # This is your Robot test image. It will run the test like if it was an external service.
    image: ipdm-harbor-dev.in.telstra.com.au/devtest/rf:latest
    container_name: mytests
    depends_on:
    - nso
    volumes:
    - "./test:/test"
    - "./results:/results"
```

## License & Maintainers

Anthony Paulin <apaulin@cisco.com> and Salbiah Mansor <smansor@cisco.com>
