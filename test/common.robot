*** Keywords ***

Prepare Environment
    [Documentation]   This test prepares the environment
    Wait Until Keyword Succeeds     5 min   30 sec    Connect to NSO
    Wait Until Keyword Succeeds     5 min   30 sec    Validate device 1 ready
    Wait Until Keyword Succeeds     5 min   30 sec    Validate device 2 ready
    Wait Until Keyword Succeeds     5 min   30 sec    Fetch SSH Keys 1
    Wait Until Keyword Succeeds     5 min   30 sec    Fetch SSH Keys 2
    Wait Until Keyword Succeeds     5 min   30 sec    Sync From device 1
    Wait Until Keyword Succeeds     5 min   30 sec    Sync From device 2

Connect to NSO
    [Documentation]   Connect to NSO
    ${auth}=  Create List  ${NSO_USERNAME}  ${NSO_PASSWORD}
    Create Session  ${NSO}  url=${NSO_URL}  headers=${NSO_HEADER}  auth=${auth}
    ${resp}=  GET On Session  ${NSO}  /restconf
    Should Be Equal As Strings  ${resp.status_code}  200

Validate device 1 ready
    [Documentation]   Check if the device devIOS-0 is up on NSO
    ${auth}=  Create List  ${NSO_USERNAME}  ${NSO_PASSWORD}
    ${resp}=  GET On Session  ${NSO}  url=/restconf/data/tailf-ncs:devices/device=devIOS-0
    Log  ${resp.text}
    Should Be Equal As Strings  ${resp.status_code}  200

Validate device 2 ready
    [Documentation]   Check if the device devIOS-1 is up on NSO
    ${auth}=  Create List  ${NSO_USERNAME}  ${NSO_PASSWORD}
    ${resp}=  GET On Session  ${NSO}  url=/restconf/data/tailf-ncs:devices/device=devIOS-1
    Log  ${resp.text}
    Should Be Equal As Strings  ${resp.status_code}  200

Sync From device 1
    [Documentation]  Sync device devIOS-0 with NSO
    ${auth}=  Create List  ${NSO_USERNAME}  ${NSO_PASSWORD}
    ${resp}=  POST On Session  ${NSO}  url=/restconf/data/tailf-ncs:devices/device=devIOS-0/sync-from
    Log  ${resp.json()}
    Should Be Equal As Strings  ${resp.status_code}  200
    Should Not Contain  ${resp.text}  false

Sync From device 2
    [Documentation]  Sync device devIOS-1 with NSO
    ${auth}=  Create List  ${NSO_USERNAME}  ${NSO_PASSWORD}
    ${resp}=  POST On Session  ${NSO}  url=/restconf/data/tailf-ncs:devices/device=devIOS-1/sync-from
    Log  ${resp.json()}
    Should Be Equal As Strings  ${resp.status_code}  200
    Should Not Contain  ${resp.text}  false

Fetch SSH Keys 1
    [Documentation]  Fetch the device devIOS-0 SSH keys
    ${auth}=  Create List  ${NSO_USERNAME}  ${NSO_PASSWORD}
    ${resp}=  POST On Session  ${NSO}  url=/restconf/data/tailf-ncs:devices/device=devIOS-0/ssh/fetch-host-keys
    Log  ${resp.json()}
    Should Be Equal As Strings  ${resp.status_code}  200
    Should Not Contain  ${resp.text}  failed

Fetch SSH Keys 2
    [Documentation]  Fetch the device devIOS-1 SSH keys
    ${auth}=  Create List  ${NSO_USERNAME}  ${NSO_PASSWORD}
    ${resp}=  POST On Session  ${NSO}  url=/restconf/data/tailf-ncs:devices/device=devIOS-1/ssh/fetch-host-keys
    Log  ${resp.json()}
    Should Be Equal As Strings  ${resp.status_code}  200
    Should Not Contain  ${resp.text}  failed