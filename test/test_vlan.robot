*** Setting ***

Library    OperatingSystem
Library    RequestsLibrary
Library    Collections
Library    String

Resource      common.robot

Documentation    Build an environment and create
...              one VLAN 1000 on GigabitEthernet
...              0/0 on 2 devices devIOS-0 and devIOS-1

Suite Setup      Prepare Environment

*** Variables ***

${NSO}           nso
${NSO_URL}       http://nso:80
${NSO_USERNAME}  admin
${NSO_PASSWORD}  admin
&{NSO_HEADER}    Content-Type=application/yang-data+json
...              Accept=application/yang-data+json
...              Authorization=Basic

*** Test Cases ***

Build VLAN testService1
    [Documentation]  Create and verify a service called testService1 with VLAN-ID 1000 and then delete it
    ${data}  Get File  resources/testService1.json
    ${resp}=  POST On Session  ${NSO}
    ...       url=/restconf/data/tailf-ncs:services  data=${data}
    Should Be Equal As Strings  ${resp.status_code}  201
    Get Service From NSO
    Validate Interfaces
    Delete service

*** Keywords ***

Get Service From NSO
    [Documentation]  Get the service from NSO
    ${resp}=  GET On Session  ${NSO}
    ...       url=/restconf/data/tailf-ncs:services/vlan:vlan=testService1/device-list
    Log  ${resp.json()}
    Should Be Equal As Strings  ${resp.status_code}  200
    Should Be Equal As Strings  ${resp.json()['vlan:device-list'][0]}  devIOS-0
    Should Be Equal As Strings  ${resp.json()['vlan:device-list'][1]}  devIOS-1

Validate Interfaces
    [Documentation]  Get the interfaces from NSO
    ${resp}=  GET On Session  ${NSO}
    ...       url=/restconf/data/tailf-ncs:devices/device=devIOS-0/config/tailf-ned-cisco-ios:interface/
    Log  ${resp.json()}
    Should Be Equal As Strings  ${resp.status_code}  200
    Should Contain  ${resp.text}  "vlans": [1000]

Delete service
    [Documentation]  Delete the service we created
    ${resp}=  DELETE On Session  ${NSO}
    ...       url=/restconf/data/tailf-ncs:services/vlan:vlan=testService1
    Should Be Equal As Strings  ${resp.status_code}  204
