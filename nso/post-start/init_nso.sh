#!/bin/bash

# This script is used to add data on startup. 
# It is loaded in /etc/ncs/post-ncs-start.d

#Load the default auth group
ncs_load -l -m /etc/ncs/post-ncs-start.d/authgroup.xml

#Onboard the 2 devices in other containers
echo "config
devices device devIOS-0
 address devIOS-0
 port 22
 authgroup default
 device-type cli ned-id cisco-ios-cli-6.40
 state admin-state unlocked
 ssh fetch-host-keys
 commit
end" | ncs_cli -Cu admin

echo "config
devices device devIOS-1
 address devIOS-1
 port 22
 authgroup default
 device-type cli ned-id cisco-ios-cli-6.40
 state admin-state unlocked
 ssh fetch-host-keys
 commit
end" | ncs_cli -Cu admin

echo "devices sync-from device [ devIOS-0 devIOS-1 ]" | ncs_cli -Cu admin
